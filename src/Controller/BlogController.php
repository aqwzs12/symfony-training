<?php


namespace App\Controller;


use App\Repository\BlogPostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BlogController
 * @package App\Controller
 * @Route("/blog")
 */
class BlogController extends AbstractController
{


    /**
     * @Route("/{page}",name="blog_list");
     */
    public function list($page = 1 , Request $request , BlogPostRepository $repository)
    {
        $limit = $request->get('limit',10);
        return $this->json(["page" => $page,
            "limit" => $limit ,
            "posts" => array_map(function ($post) {
                return $this->generateUrl("blog_post_id", ["id" => $post["id"]]);
            }, $repository->findAll())]);
    }

    /**
     * @Route("/post/{id}",name="blog_post_id");
     */
    public function post($id , BlogPostRepository $repository)
    {
        return $this->json($repository->find($id));
    }

    /**
     * @Route("/post/{slug}",name="blog_post_slug");
     */
    public function postBySlug($slug, BlogPostRepository $repository)
    {
        return $this->json($repository->findBy(["slug" => $slug]));

    }

    /**
     * @Route("/post/add" , name="blog_post_add" , methods={"POST"})
     */
    public function add(Request $request){
        $serializer = $this->get('serializer');
        $blogPost = $serializer->deserializer($request->getContent(),BlogPost::class,'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($blogPost);
        $em->flush();

        return $this->json($blogPost);
    }
}