<?php


namespace App\Service;


use Psr\Log\LoggerInterface;

class Gretting
{

    private $logger;

    /**
     * Gretting constructor.
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function greet(string $name): string
    {
        return "Hello $name";
    }

}